#!/bin/sh

if [ -z "${SUDO_USER+x}" ]; then
    printf "%s\n" "This script needs to be invoked as sudo"
    exit -1
fi

info() {
    printf "[bootstrap] %s...\n" "$@"
}

source /etc/os-release
USER_HOME=$(getent passwd "$SUDO_USER" | cut -d: -f6)

arch_or_ubuntu() {
    ( [ "${ID}" = "arch" ] || [ "${ID_LIKE}" = "arch" ] ) && $1
    ( [ "${ID}" = "ubuntu" ] || [ "${ID}" = "debian" ] || [ "${ID_LIKE}" = "debian" ] ) && $2
}

install_zsh_arch() {
    pacman -S --noconfirm zsh
}

install_zsh_ubuntu() {
    apt -y install zsh
}

install_zsh() {
    info "Installing ZSH"
    arch_or_ubuntu install_zsh_arch install_zsh_ubuntu
}

install_fzf_arch() {
    pacman -S --noconfirm fzf
}

install_fzf_ubuntu() {
    apt -y install fzf
}

install_fzf() {
    info "Installing fzf"
    arch_or_ubuntu install_fzf_arch install_fzf_ubuntu
}

install_autocomplete_arch() {
    pacman -S --noconfirm zsh-autosuggestions
}

install_autocomplete_ubuntu() {
    apt -y install zsh-autosuggestions
}

install_autocomplete() {
    info "Installing zsh-autosuggestions"
    arch_or_ubuntu install_autocomplete_arch install_autocomplete_ubuntu
}

install_highlighting_arch() {
    pacman -S --noconfirm zsh-syntax-highlighting
}

install_highlighting_ubuntu() {
    apt -y install zsh-syntax-highlighting
}

install_highlighting() {
    info "Installing zsh-syntax-highlighting"
    arch_or_ubuntu install_highlighting_arch install_highlighting_ubuntu
}

install_git_arch() {
    pacman -S --noconfirm git
}

install_git_ubuntu() {
    apt -y install git
}

install_git() {
    info "Installing git"
    arch_or_ubuntu install_git_arch install_git_ubuntu
}

clone_shincludes() {
    info "Cloning .shincludes"
    sudo -u "$SUDO_USER" git clone https://git.dog/xx/.shincludes "$USER_HOME/.shincludes"
}

clone_dots() {
    info "Cloning and installing dots"
    sudo -u "$SUDO_USER" git clone git@git.dog:xx/.dots.git "$USER_HOME/.dots" || return
    sudo -u "$SUDO_USER" sh -c "cd \"$USER_HOME/.dots\" && ./install.sh install"
}

enable_zsh() {
    info "Setting ZSH as the user's shell"
    chsh -s $(which zsh) "$SUDO_USER"
}

info "Starting bootstrap"

install_zsh
install_fzf
install_autocomplete
install_highlighting
install_git
clone_shincludes
clone_dots
enable_zsh

info "All done"
